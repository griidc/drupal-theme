<?php

/**
 * @file
 * GRIIDC template.php
 */

/**
 * Implements hook_preprocess_html()
 **/
function griidc_preprocess_html(&$variables) {
  drupal_add_css(path_to_theme() . '/style.css');
  drupal_add_css(path_to_theme() . '/css/suckerfish.css');
}

/**
 * Implements hook_preprocess_block()
 **/
function griidc_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'suckerfish') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }

  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }

  $variables['title_attributes_array']['class'][] = 'title';
}

/**
 * Implements hook_process_html()
 **/
function griidc_process_html(&$variables) {
  $variables['styles'] .= '<style type="text/css">
      #toolbar {
      }
      body.toolbar {
          padding-top: 0;
      }
      body.toolbar-drawer {
          padding-top: 0;
      }
      body {
          padding: 0;
      }
        </style>';

  $variables['styles'] .= "<!-- $variables[attributes] -->";

  $variables['styles'] .= '<style type="text/css">
              body {
                font-family: Arial, Verdana, sans-serif;
              }
           </style>';

  $variables['styles'] .= '<style type="text/css">
    #suckerfishmenu div .contextual-links-wrapper {
      display:none;
    }
    #suckerfishmenu2 div .contextual-links-wrapper {
      display:none;
    }
    </style>';

    $variables['scripts'] .= '<script type="text/javascript" src="' . $GLOBALS['base_url'] . '/' . path_to_theme() . '/js/override_toolbar.js"></script>';

    if (theme_get_setting('griidc_head')) {
        $variables['scripts'] .= "\n\n" . theme_get_setting('griidc_head') . "\n\n";
    }

}

function griidc_menu_link($variables) {
    $element = &$variables['element'];

    $pattern = '/\S+\.(png|gif|jpg)\b/i';
    if (preg_match($pattern, $element['#title'], $matches)) {
        $element['#title'] = preg_replace($pattern,
        '<div style="position:absolute; top:-1px; right:-7px"><img align="absmiddle" src = "/sites/all/themes/griidc/images/' . url($matches[0]) . '" /></div>',
        $element['#title']);
        $element['#title'] = "<div style='position:relative'>".$element['#title']."</div>";
        $element['#localized_options']['html'] = TRUE;
    }

    return theme_menu_link($variables);
}
