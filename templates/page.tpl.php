<?php
/**
 * @file
 * GRIIDC page.tpl.php
 *
 * for Default theme implementation to display a block see modules/system/page.tpl.php.
 */
?>

<div id="page-inner-wrapper">
    <div id="page"> <!-- table -->

        <div id="header-row">
            <div id="header-cell">
                <div id="header">
                    <div class="header-corner header-corner-left"></div>
                    <div class="header-corner header-corner-right"></div>
                    <?php if ($logo): ?>
                        <div id="logo-title">
                            <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" /></a>
                        </div>
                    <?php endif; ?>
                    <?php if ($page['login_logout']): ?>
                        <!-- login_logout-region-->
                        <div id="login_logout">
                            <?php print render($page['login_logout']); ?>
                        </div>
                        <!-- /login_logout-region -->
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div id="menu-row">
            <div id="menu-cell">
                <!-- GRIIDC menu -->
                <?php if ($page['suckerfish']): ?>
                    <div id="suckerfishmenu">
                        <?php print render($page['suckerfish']); ?>
                    </div>
                <?php endif; ?>
                <!-- GoMRI menu -->
                <?php if ($page['suckerfish2']): ?>
                    <div id="suckerfishmenu2">
                        <?php print render($page['suckerfish2']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div id="main-row">
            <div id="main-cell">
                <!-- CAS-top-end -->
                <div id="main"> <!-- table -->

                    <?php if ($show_messages and $messages) { ?>
                        <div class="row">
                            <div id="messages" class="cell">
                                <?php print render($messages); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($title and !$is_front and (!array_key_exists('hide_page_title',$GLOBALS) or !$GLOBALS['hide_page_title'])): ?>
                        <div class="row">
                            <div id="branding" class="cell">
                                <?php print render($title_prefix); ?>
                                <h1 class="title"><?php print render($title); ?></h1>
                                <?php print render($title_suffix); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($tabs): ?>
                        <div class="row">
                            <div class="tabs cell"><?php print render($tabs) ?></div>
                        </div>
                    <?php endif; ?>

                    <!-- CONTENT -->
                    <div class="row">
                        <?php print render($page['content']); ?>
                    </div>
                    <!-- /CONTENT -->

                </div> <!-- main table -->
                <!-- CAS-bottom-start -->
            </div> <!-- main-cell -->
        </div> <!-- main-row -->

        <div id="footer-row">
            <div id="footer-cell">
                <div id="footer">
                    <?php if ($page['footer'] || $page['footer_message']): ?>
                            <?php if ($page['footer']) { ?>
                                <div id="footer-region">
                                    <?php print render($page['footer']);?>
                                </div>
                            <?php } ?>
                            <?php if ($page['footer_message']) { ?>
                                <div id="footer-message">
                                    <?php print render($page['footer_message']) ?>
                                </div>
                            <?php  } ?>
                    <?php endif; ?>
                    <div class="footer-corner footer-corner-left"></div>
                    <div class="footer-corner footer-corner-right"></div>
                </div> <!-- footer -->
            </div> <!-- footer-cell -->
        </div> <!-- footer-row -->

    </div> <!-- page table -->
</div> <!-- page-inner-wrapper -->
