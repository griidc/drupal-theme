<?php

/**
 * @file
 * GRIIDC theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
    <?php print $head; ?>
    <title><?php print $head_title_array['name']; ?> | <?php print $head_title_array['title']; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <div id="outer-container">
        <?php if ($page_top) { ?>
            <div>
                <?php print $page_top; ?>
            </div>
        <?php } ?>
        <div>
            <div id="page-outer-wrapper">
                <?php print $page; ?>
            </div>
        </div>
        <?php if ($page_bottom) { ?>
            <div>
                <?php print $page_bottom; ?>
            </div>
        <?php } ?>
    </div>
</body>
</html>
