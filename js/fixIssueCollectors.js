var width1 = 0;
var width2 = 0;
var margin_top = 20;
var spacing = 3;

function findIssueCollectorWidths(text1,text2) {
    var found1 = false;
    var found2 = false;
    $('.atlwdg-RIGHT').each(function() {
        $(this).css('font-size','9pt');
        $(this).css('font-family','sans-serif');
        $(this).css('font-weight','normal');
        $(this).css('padding','0 5px 0 5px');
        $(this).css('border','1px solid white');
        $(this).css('border-top','0px');
        if ($(this).text() == text1) {
            width1 = $(this).width();
            found1 = true;
        }
        if ($(this).text() == text2) {
            width2 = $(this).width();
            found2 = true;
        }
    });
    if (!found1 || !found2) {
        setTimeout('findIssueCollectorWidths("' + text1 + '","' + text2 + '")',10);
    }
    else {
        splitIssueCollectors(text1,text2);
    }
}

function splitIssueCollectors(text1,text2) {
    var found1 = false;
    var found2 = false;
    toolbar_bottom = 0;
    if ($('#toolbar')) {
        toolbar_bottom = $('#toolbar').height();
    }
    $('.atlwdg-RIGHT').each(function() {
        if ($(this).text() == text1) {
            $(this).css('top',toolbar_bottom + margin_top + 'px');
            found1 = true;
        }
        if ($(this).text() == text2) {
            $(this).css('top',toolbar_bottom + margin_top + width1 + 12 + spacing + 'px');
            found2 = true;
        }
        $(this).css('color','white');
    });
   if (!found1 || !found2) setTimeout('splitIssueCollectors("' + text1 + '","' + text2 + '")',10);
}

function fixIssueCollectors(text1,text2) {
    $(document).ready(function() {
        setTimeout('findIssueCollectorWidths("' + text1 + '","' + text2 + '")',10);
    });
}
