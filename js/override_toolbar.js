(function ($) {
/**
 * Collapse the toolbar.
 */
if (Drupal.toolbar) {
    Drupal.toolbar.collapse = function() {
        var toggle_text = Drupal.t('Show shortcuts');
        $('#toolbar div.toolbar-drawer').addClass('collapsed');
        $('#toolbar a.toggle')
        .removeClass('toggle-active')
        .attr('title',  toggle_text)
        .html(toggle_text);
        $('.region-page-top').height(Drupal.toolbar.height());
        $.cookie(
            'Drupal.toolbar.collapsed',
            1,
            {
                path: Drupal.settings.basePath,
                // The cookie should "never" expire.
                expires: 36500
            }
        );
    };
}

/**
 * Expand the toolbar.
 */
if (Drupal.toolbar) {
    Drupal.toolbar.expand = function() {
        var toggle_text = Drupal.t('Hide shortcuts');
        $('#toolbar div.toolbar-drawer').removeClass('collapsed');
        $('#toolbar a.toggle')
        .addClass('toggle-active')
        .attr('title',  toggle_text)
        .html(toggle_text);
        $('.region-page-top').height(Drupal.toolbar.height());
        $.cookie(
            'Drupal.toolbar.collapsed',
            0,
            {
                path: Drupal.settings.basePath,
                // The cookie should "never" expire.
                expires: 36500
            }
        );
    };
}

})(jQuery);
