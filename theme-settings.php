<?php

/**
 * @file
 * GRIIDC theme-settings.php
 */

function griidc_form_system_theme_settings_alter(&$form, $form_state){

 $form['griidc_head'] = array(
    '#type' => 'textarea',
    '#title' => t('Head'),
    '#description' => t('Add extra stuff to be included in the head section of every page.'),
    '#default_value' => theme_get_setting('griidc_head'),
    '#rows' => 12,
  );

  return $form;
}
